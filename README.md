This project shows a way to automate 2 tasks:
Adidas API test automation challenge
Adidas Selenium test automation challenge

Stack:
Gradle
Java 8
Spring 5 (to handle the properties and contexts)
JUnit5 aka "Jupiter" (to handle test-suites and test running)
Hamcrest (to handle assertions)
Rest-Assured framework (to handle HTTP requests)
Jackson (to cast Jsons into POJOs)
WebDriver (to handle interactions with browser)


No setup is needed, only Java 8 installed.

A command-line parameter "currentEnv" is used just to show that there is a way to run tests on different environments.
PageObject pattern is used to hide "hardcore" operations with web pages and to make tests look simple and neat.
There is a mid-layer "*Methods" to separate widely used (or re-used) methods.   

To run an API testSuite (API Task) from command-line:
*gradlew clean testCustom -PcurrentEnv=dev*

To run a Selenium testSuite (2nd Task) from command-line:
*gradlew clean testGui -PcurrentEnv=dev*

To run all tests from command-line:
*gradlew clean testAll -PcurrentEnv=dev*

To run a single test of a testSuite from command-line:
*gradlew clean testCustom --tests=tests.WeatherByNameTest -PcurrentEnv=dev*

To run a test in the IDE set system property "currentEnv" in VM options: -DcurrentEnv=dev