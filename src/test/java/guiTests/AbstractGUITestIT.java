package guiTests;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ContextConfiguration(classes = {GUITestConfig.class})
public abstract class AbstractGUITestIT {

    @Value("${hostUrl}")
    protected String hostUrl;

    @Autowired
    protected WebDriver driver;

    @Value("${webDriverWait}")
    protected int webDriverWait;

    public Boolean isOnPage(String url) {
        return driver.getCurrentUrl().endsWith(url);
    }

    public void refresh() {
        driver.navigate().refresh();
    }
}
