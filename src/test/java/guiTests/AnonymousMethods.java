package guiTests;

import guiTests.pageObjects.CommentErrorPage;
import guiTests.pageObjects.MainPage;
import guiTests.pageObjects.SamplePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AnonymousMethods extends AbstractGUITestIT {

    @Autowired
    private MainPage mainPage;

    @Autowired
    private SamplePage samplePage;

    @Autowired
    private CommentErrorPage commentErrorPage;

    public MainPage openMainPage() {
        mainPage.open().waitForLoad();
        return mainPage;
    }

    public MainPage onMainPage() {
        mainPage.waitForLoad();
        return mainPage;
    }


    public SamplePage onSamplePage() {
        samplePage.waitForLoad();
        return samplePage;
    }

    public CommentErrorPage onCommentErrorPage() {
        commentErrorPage.waitForLoad();
        return commentErrorPage;
    }
}
