package guiTests;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource("classpath:gui.properties")
@ComponentScan(basePackageClasses = {GUITestConfig.class})
public class GUITestConfig {

    @Value("${hostUrl}")
    protected String hostUrl;

    @Value("${browser}")
    private String browserType;

    @Bean
    public static PropertySourcesPlaceholderConfigurer
    propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean(destroyMethod = "quit")
    public WebDriver getDriver() {
        switch (browserType) {
            case "Chrome": {
                ChromeDriverManager.getInstance().setup();
                ChromeOptions options = new ChromeOptions();
//                to enable headless chrome
//                options.addArguments("--headless");
//                options.addArguments("--disable-gpu");
                return new ChromeDriver(options);
            }
            case "Firefox": {
                FirefoxDriverManager.getInstance().setup();
                return new FirefoxDriver();
            }
            default: {
                ChromeDriverManager.getInstance().setup();
                ChromeOptions options = new ChromeOptions();
                return new ChromeDriver(options);
            }
        }
    }
}
