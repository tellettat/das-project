package guiTests;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Scenario:
 * 1. Open http://store.demoqa.com
 * 2. Navigate to 'Sample page'
 * 3. Enter a comment with a wrong email
 * 4. Check Error is displayed
 * 5. Navigate back
 * 6. Enter a comment with a correct email
 * 7. Check Comment is received
 * 8. Shutdown browser
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Category(GUITest.class)
public class WrongEmailTest extends AbstractGUITestIT {

    @Autowired
    private AnonymousMethods user;

    @Before
    public void beforeTest() {
        user.openMainPage();
    }

    private String random = RandomStringUtils.random(10, "0123456789asdfghjkl");

    @Test
    public void verifyElementsOnApiKeysPage() {
        String text = "This is a new comment: " + random;
        String name = "anonymous" + random;
        user.onMainPage()
                .navigateToSamplePage();
        user.onSamplePage()
                .enterComment(text)
                .enterName(name)
                .enterEmail("random text")
                .clickSubmit();
        user.isOnPage("/wp-comments-post.php");
        user.onCommentErrorPage()
                .verifyErrorText("ERROR: please enter a valid email address.")
                .navigateBack();
        user.onSamplePage()
                .enterComment(text)
                .enterName(name)
                .enterEmail("mail" + random + "@valid.com")
                .clickSubmit();
        user.onSamplePage()
                .verifyCommentReceived("Your comment is awaiting moderation.", name, text);
    }
}
