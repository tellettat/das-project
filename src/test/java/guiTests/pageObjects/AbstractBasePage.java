package guiTests.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import guiTests.MyPageObject;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;

@Component
@MyPageObject
public abstract class AbstractBasePage {

    @Autowired
    protected WebDriver driver;

    @Value("${webDriverWait}")
    protected int webDriverWait;

    @Value("${hostUrl}")
    protected String hostUrl;

    public void waitForLoad() {
        new WebDriverWait(driver, webDriverWait)
                .until(ExpectedConditions.visibilityOf(getBasicElement()));
    }

    public abstract AbstractBasePage open();

    public abstract WebElement getBasicElement();

    public void isOn(String url) {
        assertThat("Current url differs from the expected. Expected url ends with " + url, driver.getCurrentUrl(), endsWith(url));
    }

    public void maximizeWindow() {
        driver.manage().window().maximize();
    }
}
