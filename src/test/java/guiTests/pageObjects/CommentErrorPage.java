package guiTests.pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import guiTests.MyPageObject;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@Component
@MyPageObject
public class CommentErrorPage extends AbstractBasePage {

    @FindBy(linkText = "« Back")
    private WebElement backLink;

    @FindBy(xpath = "//body/p[2]")
    private WebElement errorText;


    @Override
    public WebElement getBasicElement() {
        new WebDriverWait(driver, webDriverWait)
                .until(ExpectedConditions.visibilityOf(backLink));
        return backLink;
    }

    @Override
    public AbstractBasePage open() {
        driver.get(hostUrl + "/wp-comments-post.php");
        return this;
    }

    @Autowired
    private SamplePage samplePage;

    public SamplePage navigateBack() {
        backLink.click();
        samplePage.waitForLoad();
        return samplePage;
    }

    public CommentErrorPage verifyErrorText(String text) {
        assertThat("Expected text fidders", errorText.getText(), equalTo(text));
        return this;
    }
}
