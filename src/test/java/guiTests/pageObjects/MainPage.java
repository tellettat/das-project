package guiTests.pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import guiTests.MyPageObject;

@Component
@MyPageObject
public class MainPage extends AbstractBasePage {

    @FindBy(xpath = "//li[@id='menu-item-54']/a")
    private WebElement samplePageLink;

    @Override
    public WebElement getBasicElement() {
        new WebDriverWait(driver, webDriverWait)
                .until(ExpectedConditions.visibilityOf(samplePageLink));
        return samplePageLink;
    }

    @Override
    public AbstractBasePage open() {
        driver.get(hostUrl);
        return this;
    }

    @Autowired
    private SamplePage samplePage;

    public SamplePage navigateToSamplePage() {
        samplePageLink.click();
        samplePage.waitForLoad();
        return samplePage;
    }
}
