package guiTests.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Component;
import guiTests.MyPageObject;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;


@Component
@MyPageObject
public class SamplePage extends AbstractBasePage {
    @FindBy(id = "submit")
    private WebElement submitComment;
    @FindBy(id ="comment")
    private WebElement commentInput;
    @FindBy(id ="author")
    private WebElement nameInput;
    @FindBy(id ="email")
    private WebElement emailInput;

    @FindBy(xpath = "//ol[contains(@class, 'commentlist')]//div[contains(@class, 'comment_container')]/em")
    private List<WebElement> commetnStatuses;

    @FindBy(xpath = "//ol[contains(@class, 'commentlist')]//div[contains(@class, 'comment-author')]/cite")
    private List<WebElement> commentsAuthors;

    @FindBy(xpath = "//ol[contains(@class, 'commentlist')]//div[contains(@class, 'comment-body')]/p")
    private List<WebElement> commentsTexts;


    @Override
    public WebElement getBasicElement() {
        new WebDriverWait(driver, webDriverWait)
                .until(ExpectedConditions.visibilityOf(submitComment));
        return submitComment;
    }

    @Override
    public AbstractBasePage open() {
        driver.get(hostUrl + "/sample-page/");
        return this;
    }

    public SamplePage enterComment(String text) {
        commentInput.clear();
        commentInput.sendKeys(text);
        return this;
    }

    public SamplePage enterName(String text) {
        nameInput.clear();
        nameInput.sendKeys(text);
        return this;
    }

    public SamplePage enterEmail(String text) {
        emailInput.clear();
        emailInput.sendKeys(text);
        return this;
    }

    public void clickSubmit() {
        submitComment.click();
    }

    public SamplePage verifyCommentReceived(String status, String name, String text) {
        assertThat(new WebDriverWait(driver, webDriverWait)
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//ol[contains(@class, 'commentlist')]/li"))).size(), equalTo(1));
        assertThat("Expected status differs", commetnStatuses.get(0).getText(), equalTo(status));
        assertThat("Expected name differs", commentsAuthors.get(0).getText(), equalTo(name));
        assertThat("Expected text differs", commentsTexts.get(0).getText(), equalTo(text));
        return this;
    }
}
