package models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class CityObject {
    private String id;
    private String name;
    private CoordObject coord;
    private String country;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public CoordObject getCoord() {
        return coord;
    }

    public String getCountry() {
        return country;
    }
}
