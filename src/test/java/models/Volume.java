package models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Volume {

    private double value;

    @JsonSetter("3h")
    public void setValue(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
