package models.responses;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class ErrorResponse {
    private String cod;
    private String message;

    public String getCod() {
        return cod;
    }

    public String getMessage() {
        return message;
    }
}
