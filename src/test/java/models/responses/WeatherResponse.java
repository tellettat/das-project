package models.responses;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import models.*;

import java.util.List;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class WeatherResponse {
    private CoordObject coord;
    private List<Weather> weather;
    private String base;
    private MainWeather main;
    private int visibility;
    private Wind wind;
    private Clouds clouds;
    private Volume rain;
    private Volume snow;
    private long dt;
    private Sys sys;
    private int id;
    private String name;
    private int cod;

    public CoordObject getCoord() {
        return coord;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public String getBase() {
        return base;
    }

    public MainWeather getMain() {
        return main;
    }

    public int getVisibility() {
        return visibility;
    }

    public Wind getWind() {
        return wind;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public long getDt() {
        return dt;
    }

    public Sys getSys() {
        return sys;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getCod() {
        return cod;
    }

    public Volume getRain() {
        return rain;
    }

    public Volume getSnow() {
        return snow;
    }
}
