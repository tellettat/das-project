package tests;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ContextConfiguration(classes = {TestConfig.class})
public abstract class AbstractTestIT {

    @Value("${weather.apiKey}")
    private String apiKey;

    /**
     * Set base url and a required query parameter APPID
     * @param url
     * @return
     */
    protected RequestSpecification setUpBaseUrl(String url) {
        RestAssured.baseURI = url;
        return RestAssured.given().log().all().queryParam("APPID", apiKey);
    }
}
