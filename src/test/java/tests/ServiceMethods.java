package tests;

import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import models.requests.CreatePostRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

import static java.util.Objects.isNull;

@Component
public class ServiceMethods extends AbstractTestIT {
    @Value("${hostUrl}")
    private String serviceUrl;
    @Value("${service.apiPath}")
    protected String apiPath;

    private RequestSpecification httpRequest;

    /**
     * Send GET request to get weather by city name
     * @param query a text in format city_name[,country]
     * @param headers necessary headers
     * @return
     */
    public Response getWeatherByName(String query, Map<String, String> headers) {
        httpRequest = setUpBaseUrl(serviceUrl);
        httpRequest.headers(headers);
        if(!isNull(query)) {
            httpRequest.queryParam("q", query);
        }
        return httpRequest.request(Method.GET, apiPath);
    }

    public Response getWeatherById(String id, Map<String, String> headers) {
        httpRequest = setUpBaseUrl(serviceUrl);
        httpRequest.headers(headers);
        if(!isNull(id)) {
            httpRequest.queryParam("id", id);
        }
        return httpRequest.request(Method.GET, apiPath);
    }
}
