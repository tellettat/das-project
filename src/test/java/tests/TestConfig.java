package tests;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import utils.UtilsMethods;

@Configuration
@ComponentScan(basePackageClasses = {TestConfig.class, UtilsMethods.class})
@PropertySource("classpath:${currentEnv}.properties")
public class TestConfig {
    @Bean
    public static PropertySourcesPlaceholderConfigurer
    propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Value("${env}")
    private String env;

    @Bean
    public String currentEnv() {
        System.out.println("TestConfig: current env is " + env);
        return env;
    }
}
