package tests;

import io.restassured.response.Response;
import models.responses.ErrorResponse;
import models.responses.WeatherResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.isNull;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

/**
 * A set of tests to test getWeatherByName endpoint
 * There are no requirements on that, so I selected a few just to show the capabilities of Rest-Assured and Hamcrest
 * Note that hardcoded CITY, CITY_ID, COUNTRY may be organized as the test params. In that way the test can be ran against large amount of the input values.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Category({IncludeTest.class})
public class WeatherByNameTest extends AbstractTestIT {
    @Autowired
    private ServiceMethods serviceMethods;

    private Map<String, String> headers;

    private static final String CITY = "London";
    private static final int CITY_ID = 2643743;
    private static final String COUNTRY = "GB";

    @Before
    public void setUp() {
        headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Content-Type", "application/json");
    }

    @After
    public void cleanUp() {
        System.out.println("Farewell my friend!");
    }

    @Test
    public void verifyAnonymousCanGetWeatherByCityOnly() {
        Response response = serviceMethods.getWeatherByName(CITY, headers);
        assertThat("Expected 200 from endpoint\n" + response.prettyPrint(), response.getStatusCode(), is(200));
        WeatherResponse weatherResponse = response.as(WeatherResponse.class);
        assertThat("Expected id differs", weatherResponse.getId(), is(CITY_ID));
        assertThat("Expected name differs", weatherResponse.getName(), equalTo(CITY));
        assertThat("Expected coord differs", weatherResponse.getCoord(), notNullValue());
        assertThat("Expected coord.lat differs", weatherResponse.getCoord().getLat(), notNullValue());
        assertThat("Expected coord.lon differs", weatherResponse.getCoord().getLon(), notNullValue());
        assertThat("Expected weather differs", weatherResponse.getWeather(), notNullValue());
        weatherResponse.getWeather().forEach(weather -> {
            assertThat("Expected weather.id differs", weather.getId(), notNullValue());
            assertThat("Expected weather.main differs", weather.getMain(), notNullValue());
            assertThat("Expected weather.description differs", weather.getDescription(), notNullValue());
            assertThat("Expected weather.icon differs", weather.getIcon(), notNullValue());
        });
        assertThat("Expected base differs", weatherResponse.getBase(), notNullValue());
        assertThat("Expected main differs", weatherResponse.getMain(), notNullValue());
        assertThat("Expected main.temp differs", weatherResponse.getMain().getTemp(), notNullValue());
        assertThat("Expected main.pressure differs", weatherResponse.getMain().getPressure(), allOf(notNullValue(), greaterThanOrEqualTo(0.0)));
        assertThat("Expected main.humidity differs", weatherResponse.getMain().getHumidity(), allOf(notNullValue(), greaterThanOrEqualTo(0), lessThanOrEqualTo(100)));
        assertThat("Expected main.temp_min differs", weatherResponse.getMain().getTemp_min(), notNullValue());
        assertThat("Expected main.temp_max differs", weatherResponse.getMain().getTemp_max(), notNullValue());
        assertThat("Expected visibility differs", weatherResponse.getVisibility(), notNullValue());
        assertThat("Expected wind differs", weatherResponse.getWind(), notNullValue());
        assertThat("Expected wind.deg differs", weatherResponse.getWind().getDeg(), allOf(notNullValue(), greaterThanOrEqualTo(0), lessThanOrEqualTo(360)));
        assertThat("Expected wind.gust differs", weatherResponse.getWind().getGust(), allOf(notNullValue(), greaterThanOrEqualTo(0.0)));
        assertThat("Expected wind.speed differs", weatherResponse.getWind().getSpeed(), allOf(notNullValue(), greaterThanOrEqualTo(0.0)));
        assertThat("Expected clouds differs", weatherResponse.getClouds(), notNullValue());
        assertThat("Expected clouds.all differs", weatherResponse.getClouds().getAll(), allOf(notNullValue(), greaterThanOrEqualTo(0), lessThanOrEqualTo(100)));
        assertThat("Expected dt differs", weatherResponse.getDt(), notNullValue());
        assertThat("Expected sys differs", weatherResponse.getSys(), notNullValue());
        assertThat("Expected sys.country differs", weatherResponse.getSys().getCountry(), equalTo(COUNTRY));
        assertThat("Expected sys.sunrise differs", weatherResponse.getSys().getSunrise(), notNullValue());
        assertThat("Expected sys.sunset differs", weatherResponse.getSys().getSunset(), notNullValue());
        if (!isNull(weatherResponse.getRain())) {
            assertThat("Expected show.3h differs", weatherResponse.getRain().getValue(), allOf(notNullValue(), greaterThanOrEqualTo(0.0)));
        }
        if (!isNull(weatherResponse.getSnow())) {
            assertThat("Expected show.3h differs", weatherResponse.getSnow().getValue(), allOf(notNullValue(), greaterThanOrEqualTo(0.0)));
        }
    }

    @Test
    public void verifyAnonymousCanGetWeatherByCityAndCountry() {
        Response response = serviceMethods.getWeatherByName(CITY + "," + COUNTRY, headers);
        assertThat("Expected 200 from endpoint\n" + response.prettyPrint(), response.getStatusCode(), is(200));
        WeatherResponse weatherResponse = response.as(WeatherResponse.class);
        assertThat("Expected id differs", weatherResponse.getId(), is(CITY_ID));
        assertThat("Expected name differs", weatherResponse.getName(), equalTo(CITY));
        assertThat("Expected coord differs", weatherResponse.getCoord(), notNullValue());
        assertThat("Expected coord.lat differs", weatherResponse.getCoord().getLat(), notNullValue());
        assertThat("Expected coord.lon differs", weatherResponse.getCoord().getLon(), notNullValue());
        assertThat("Expected weather differs", weatherResponse.getWeather(), notNullValue());
        weatherResponse.getWeather().forEach(weather -> {
            assertThat("Expected weather.id differs", weather.getId(), notNullValue());
            assertThat("Expected weather.main differs", weather.getMain(), notNullValue());
            assertThat("Expected weather.description differs", weather.getDescription(), notNullValue());
            assertThat("Expected weather.icon differs", weather.getIcon(), notNullValue());
        });
        assertThat("Expected base differs", weatherResponse.getBase(), notNullValue());
        assertThat("Expected main differs", weatherResponse.getMain(), notNullValue());
        assertThat("Expected main.temp differs", weatherResponse.getMain().getTemp(), notNullValue());
        assertThat("Expected main.pressure differs", weatherResponse.getMain().getPressure(), allOf(notNullValue(), greaterThanOrEqualTo(0.0)));
        assertThat("Expected main.humidity differs", weatherResponse.getMain().getHumidity(), allOf(notNullValue(), greaterThanOrEqualTo(0), lessThanOrEqualTo(100)));
        assertThat("Expected main.temp_min differs", weatherResponse.getMain().getTemp_min(), notNullValue());
        assertThat("Expected main.temp_max differs", weatherResponse.getMain().getTemp_max(), notNullValue());
        assertThat("Expected visibility differs", weatherResponse.getVisibility(), notNullValue());
        assertThat("Expected wind differs", weatherResponse.getWind(), notNullValue());
        assertThat("Expected wind.deg differs", weatherResponse.getWind().getDeg(), allOf(notNullValue(), greaterThanOrEqualTo(0), lessThanOrEqualTo(360)));
        assertThat("Expected wind.gust differs", weatherResponse.getWind().getGust(), allOf(notNullValue(), greaterThanOrEqualTo(0.0)));
        assertThat("Expected wind.speed differs", weatherResponse.getWind().getSpeed(), allOf(notNullValue(), greaterThanOrEqualTo(0.0)));
        assertThat("Expected clouds differs", weatherResponse.getClouds(), notNullValue());
        assertThat("Expected clouds.all differs", weatherResponse.getClouds().getAll(), allOf(notNullValue(), greaterThanOrEqualTo(0), lessThanOrEqualTo(100)));
        assertThat("Expected dt differs", weatherResponse.getDt(), notNullValue());
        assertThat("Expected sys differs", weatherResponse.getSys(), notNullValue());
        assertThat("Expected sys.country differs", weatherResponse.getSys().getCountry(), equalTo(COUNTRY));
        assertThat("Expected sys.sunrise differs", weatherResponse.getSys().getSunrise(), notNullValue());
        assertThat("Expected sys.sunset differs", weatherResponse.getSys().getSunset(), notNullValue());
        if (!isNull(weatherResponse.getRain())) {
            assertThat("Expected show.3h differs", weatherResponse.getRain().getValue(), allOf(notNullValue(), greaterThanOrEqualTo(0.0)));
        }
        if (!isNull(weatherResponse.getSnow())) {
            assertThat("Expected show.3h differs", weatherResponse.getSnow().getValue(), allOf(notNullValue(), greaterThanOrEqualTo(0.0)));
        }
    }

    @Test
    public void verify404WhenGetWeatherByCityAndForeignCountry() {
        Response response = serviceMethods.getWeatherByName(CITY + ",RU", headers);
        assertThat("Expected 404 from endpoint\n" + response.prettyPrint(), response.getStatusCode(), is(404));
        assertThat("Expected cod differs", response.as(ErrorResponse.class).getCod(), equalTo("404"));
        assertThat("Expected message differs", response.as(ErrorResponse.class).getMessage(), equalTo("city not found"));
    }

    @Test
    public void verify404WhenGetWeatherByUnknownCity() {
        Response response = serviceMethods.getWeatherByName(COUNTRY, headers);
        assertThat("Expected 404 from endpoint\n" + response.prettyPrint(), response.getStatusCode(), is(404));
        assertThat("Expected cod differs", response.as(ErrorResponse.class).getCod(), equalTo("404"));
        assertThat("Expected message differs", response.as(ErrorResponse.class).getMessage(), equalTo("city not found"));
    }

    @Test
    public void verifyAnonymousCanGetWeatherByUnknownCountry() {
        Response response = serviceMethods.getWeatherByName(CITY + "," + "random", headers);
        assertThat("Expected 200\n" + response.prettyPrint(), response.getStatusCode(), is(200));
        WeatherResponse weatherResponse = response.as(WeatherResponse.class);
        assertThat("Expected id differs", weatherResponse.getId(), is(CITY_ID));
        assertThat("Expected name differs", weatherResponse.getName(), equalTo(CITY));
        assertThat("Expected sys.country differs", weatherResponse.getSys().getCountry(), equalTo(COUNTRY));
    }
}
