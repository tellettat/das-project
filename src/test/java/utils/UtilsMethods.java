package utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.restassured.response.Response;
import org.springframework.stereotype.Component;

@Component
public class UtilsMethods {
    public JsonObject parseResponseToJson(Response response) {
        JsonParser parser = new JsonParser();
        return parser.parse(response.asString()).getAsJsonObject();
    }

    public JsonArray parseResponseToJsonArray(Response response) {
        JsonParser parser = new JsonParser();
        return parser.parse(response.asString()).getAsJsonArray();
    }
}
